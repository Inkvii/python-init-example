from world.ICountry import ICountry


class Slovakia(ICountry):
    def __init__(self):
        super().__init__("Slovakia", 7123456)

    def sayHi(self) -> str:
        return "Ahojte"
