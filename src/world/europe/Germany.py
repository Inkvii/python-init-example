from world.ICountry import ICountry


class Germany(ICountry):
    def __init__(self):
        super().__init__("Germany", 4087899878)

    def sayHi(self) -> str:
        return "Hallo"
