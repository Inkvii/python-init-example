from world.ICountry import ICountry


class French(ICountry):
    def __init__(self):
        super().__init__("French", 24979978)

    def sayHi(self) -> str:
        return "Bonjour"
