from world.ICountry import ICountry


class Czech(ICountry):
    def __init__(self):
        super().__init__("Czechia", 10123456)

    def sayHi(self) -> str:
        return "Ahoj"
