from world.ICountry import ICountry


class Japan(ICountry):
    def __init__(self):
        super().__init__("Japan", 509998888)

    def sayHi(self) -> str:
        return "Konichiva"
