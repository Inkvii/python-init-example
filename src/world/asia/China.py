from world.ICountry import ICountry


class China(ICountry):
    def __init__(self):
        super().__init__("China", 1123456788)

    def sayHi(self) -> str:
        return "Ni hao"
