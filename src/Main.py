from typing import List

from world import Czech, ICountry, Japan, Slovakia

if __name__ == '__main__':
    print("-------------------------------")
    list_of_countries: List[ICountry] = [Czech(), Japan(), Slovakia()]

    for country in list_of_countries:
        print(country.sayHi())
