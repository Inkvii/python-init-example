import os
import os.path
from os import DirEntry
from typing import List


def create_init_files(current_dir):
    """
    1) get list of directories
    2) cycle through directory from step 1 and go inside
    3) if there are no folders, create __init__.py file that contains imports of all files with .py
    :param path:
    :return:
    """

    subfolders = [file.path for file in os.scandir(current_dir) if
                  (file.is_dir() and not file.path.endswith("__pycache__"))]
    print(f"Subfolders: {subfolders}")

    for subfolder in subfolders:
        if len(subfolder) > 0:
            create_init_files(subfolder)

        # We are at the bottom folder here - get list of .py files
        python_file_list: List[DirEntry] = [file for file in os.scandir(subfolder) if isAllowedFile(file)]
        print(f"List of .py files in {subfolder} is: {python_file_list}")

        init_file = open(os.path.join(subfolder, "__init__.py"), "w")

        for file in python_file_list:
            line: str = f"from .{file.name.replace('.py', '')} import *\n"
            init_file.write(line)

        init_file.close()


def isAllowedFile(file: DirEntry) -> bool:
    isDirectory = file.is_dir() and not file.name.endswith("__pycache__")
    isFile = file.name.endswith(".py") and not file.name.endswith("__init__.py")

    return isDirectory or isFile


if __name__ == '__main__':
    current_dir = os.getcwd()

    create_init_files(current_dir)
